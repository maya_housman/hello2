import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AuthService } from '../auth.service';
import { CustomersService } from '../customers.service';
import { Customer } from '../interfaces/customer';

@Component({
  selector: 'addcustomerform',
  templateUrl: './add-customer-form.component.html',
  styleUrls: ['./add-customer-form.component.css']
})
export class AddCustomerFormComponent implements OnInit {

 
  name:string;  
  education:number;
  salary:number;
  id:string; 
  @Output() update = new EventEmitter<Customer>();
  @Output() closeEdit = new EventEmitter<null>();
   
  

  updateParent(){
    let customer:Customer = {id:this.id, name:this.name, education:this.education, salary:this.salary};
    this.update.emit(customer); 
  }

  tellParentToClose(){
    this.closeEdit.emit(); 
  }


  constructor() { }

  ngOnInit(): void {
  }

}