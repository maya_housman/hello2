import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { CustomersService } from '../customers.service';
import { Customer } from '../interfaces/customer';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  customers$; 
  customers:Customer[];
  userId:string;
  editstate = [];
  salary:number;
  education:number;
  category:string;
  addCustomerFormOPen = false;
  

  constructor(private customersService:CustomersService, public authService:AuthService) { }

  deleteCustomer(id:string){
    this.customersService.deleteCustomer(this.userId,id); 
  }

  update(customer:Customer){
    this.customersService.updateCustomer(this.userId, customer.id, customer.name, customer.education, customer.salary);
  }

  add(customer:Customer){
    this.customersService.addCustomer(this.userId ,customer.name, customer.salary, customer.education); 
  }

  classify(customer:Customer){
    this.customersService.classify(customer.education, customer.salary).subscribe(
      res => {
        console.log(res);
        customer.category = this.customersService.categories[res];
      }
    )
  }


  ngOnInit(): void {
    //this.booksService.addBooks(); 
    //this.books$ = this.booksService.getBooks();
    //this.books$.subscribe(books => this.books = books);
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId); 
        this.customers$ = this.customersService.getCustomers(this.userId);  
      }
    )

  }

}

