export interface Customer {
    id:string,
    name:string,
    education:number,
    salary:number,
    category?:string
}
    
