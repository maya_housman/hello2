import { getMatScrollStrategyAlreadyAttachedError } from '@angular/cdk/overlay/scroll/scroll-strategy';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  constructor(private db:AngularFirestore,private http:HttpClient) { }

  private url = "https://obfo0f0gka.execute-api.us-east-1.amazonaws.com/beta" 
  categories:object = {0.3:'no', 0.5:'no', 0.6:'yes', 0.7:'yes', 0.8:'yes', 0.9:'yes'};

  classify(education:number, salary:number){
    let json = {
"education":education,
"salary":salary
}
let result = JSON.stringify(json);
return this.http.post<any>(this.url, result).pipe(
map(res => {
 console.log(res);
 let final:string = res.result;
 console.log(final);
 return final; 
})
)

}

  customerCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 

  addCustomer(userId:string,name:string,salary:number,education:number){
    const customer = {name:name, salary:salary, education:education}; 
    this.userCollection.doc(userId).collection('customers').add(customer);
  }

  deleteCustomer(Userid:string, id:string){
    this.db.doc(`users/${Userid}/customers/${id}`).delete(); 
  } 

  updateCustomer(userId:string,id:string,name:string,education:number,salary:number){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        name:name,
        education:education,
        salary:salary
      }
    )
  }

  
  public getCustomers(userId){
    this.customerCollection = this.db.collection(`users/${userId}/customers`); 
    return this.customerCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data(); 
          data.id = document.payload.doc.id;
          return data; 
        }
      )
    ))
    
  } }
