import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Customer } from '../interfaces/customer';

@Component({
  selector: 'customerform',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {

  @Input() name:string;  
  @Input() education:number;
  @Input() salary:number;
  @Input() id:string; 
  @Output() update = new EventEmitter<Customer>();
  @Output() closeEdit = new EventEmitter<null>();
   
  updateParent(){
    let customer:Customer = {id:this.id, name:this.name, education:this.education, salary:this.salary};
    this.update.emit(customer); 
  }

  tellParentToClose(){
    this.closeEdit.emit(); 
  }


  constructor() { }

  ngOnInit(): void {
  }

}